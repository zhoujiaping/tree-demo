package org.example.tree;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.*;
import java.util.stream.Collectors;

/**
 *
 */
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TreeHelper<CODE, NODE> {
    Supplier<NODE> nodeCreator;
    Function<NODE, CODE> codeGetter;
    BiConsumer<NODE, CODE> codeSetter;
    Function<NODE, CODE> parentCodeGetter;
    BiConsumer<NODE, CODE> parentCodeSetter;
    Function<NODE, Collection<NODE>> childrenGetter;
    BiConsumer<NODE, Collection<NODE>> childrenSetter;
    Supplier<Collection<NODE>> childrenCollectionFactory = ArrayList::new;

    /**
     * 有的场景，并不是一颗树，而是一个森林。
     * 为了能够应用该类的其他方法，需要一颗虚拟节点，作为根节点。
     * 其中nodes是一个森林。
     * dummytreeCode为虚拟根节点的code。
     * <p>
     * !该方法会修改森林的各个根节点，设置其parentCode。
     */
    public NODE dummytree(CODE dummytreeCode, List<NODE> nodes) {
        NODE dummytree = nodeCreator.get();
        codeSetter.accept(dummytree, dummytreeCode);
        childrenSetter.accept(dummytree, nodes);
        nodes.forEach(node -> parentCodeSetter.accept(node, dummytreeCode));
        return dummytree;
    }

    /**
     * 将一颗树扁平化。得到一个节点列表。
     *
     * @param tree 根节点
     * @return
     */
    public List<NODE> flatten(NODE tree) {
        List<NODE> flatten = new ArrayList<>();
        bfsAll(tree, (node, level) -> flatten.add(node));
        return flatten;
    }

    /**
     * 使用广度优先遍历算法遍历一颗树。也就是从上到下，逐层遍历，每一层从左到右遍历。
     * 如果树中存在两个code相同的节点，会抛异常。
     *
     * @param tree 根节点
     * @param fn
     */
    public void bfsAll(NODE tree, BiConsumer<NODE, Integer> fn) {
        bfs(tree, (node, level) -> {
            fn.accept(node, level);
            return true;
        });
    }

    public void bfs(NODE tree, BiFunction<NODE, Integer, Boolean> fn) {
        //检测循环引用
        Set<CODE> acceptedCodes = new HashSet<>();
        LinkedList<NODE> queue1 = new LinkedList<>();
        LinkedList<NODE> queue2 = new LinkedList<>();
        int level = 0;
        queue1.add(tree);
        NODE node;
        Collection<NODE> children;
        while (!queue1.isEmpty()) {
            node = queue1.poll();
            if (!acceptedCodes.add(codeGetter.apply(node))) {
                throw new RuntimeException("multiple code: " + codeGetter.apply(node) + ", maybe there is circular reference!");
            }
            children = childrenGetter.apply(node);
            if (!fn.apply(node, level)) {
                return;
            }
            if (children != null) {
                queue2.addAll(children);
            }
            if (queue1.isEmpty()) {
                queue1 = queue2;
                queue2 = new LinkedList<>();
                level++;
            }
        }
    }

    /**
     * 深度优先（先序）算法 遍历树
     *
     * @param tree
     * @param fn
     */
    public void dfsWithPreOrder(NODE tree, BiFunction<NODE, Integer, Boolean> fn) {
        dfsWithPreOrder0(tree, fn, new HashSet<>(), 0);
    }

    public void dfsAllWithPreOrder(NODE tree, BiConsumer<NODE, Integer> fn) {
        dfsWithPreOrder0(tree, (node, level) -> {
            fn.accept(node, level);
            return true;
        }, new HashSet<>(), 0);
    }

    private boolean dfsWithPreOrder0(NODE tree, BiFunction<NODE, Integer, Boolean> fn, Set<CODE> visitedCodes, int level) {
        if (!visitedCodes.add(codeGetter.apply(tree))) {
            throw new RuntimeException("multiple code: " + codeGetter.apply(tree) + ", maybe there is circular reference!");
        }
        Collection<NODE> children = childrenGetter.apply(tree);
        if (!fn.apply(tree, level)) {
            return false;
        }
        if (children != null) {
            for (NODE child : children) {
                if (!dfsWithPreOrder0(child, fn, visitedCodes, level + 1)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 深度优先（后序）算法遍历树
     *
     * @param tree
     * @param fn
     */
    public void dfsAllWithPostOrder(NODE tree, BiConsumer<NODE, Integer> fn) {
        dfsWithPostOrder0(tree, (node, level) -> {
            fn.accept(node, level);
            return true;
        }, new HashSet<>(), 0);
    }

    public void dfsWithPostOrder(NODE tree, BiFunction<NODE, Integer, Boolean> fn) {
        dfsWithPostOrder0(tree, fn, new HashSet<>(), 0);
    }

    private boolean dfsWithPostOrder0(NODE tree, BiFunction<NODE, Integer, Boolean> fn, Set<CODE> acceptedCodes, int level) {
        if (!acceptedCodes.add(codeGetter.apply(tree))) {
            throw new RuntimeException("multiple code: " + codeGetter.apply(tree) + ", maybe there is circular reference!");
        }
        Collection<NODE> children = childrenGetter.apply(tree);
        if (children != null) {
            for (NODE child : children) {
                if (!dfsWithPostOrder0(child, fn, acceptedCodes, level + 1)) {
                    return false;
                }
            }
        }
        return fn.apply(tree, level);
    }

    /**
     * 层序遍历。和bfs不同的是，每次消费的是一层。bfs每次消费的是一个节点。
     *
     * @param tree
     * @param fn
     */
    public void bfsByLevel(NODE tree, BiConsumer<List<NODE>, Integer> fn) {
        bfsByLevel(tree, (node, level) -> {
            fn.accept(node, level);
            return true;
        });
    }

    public void bfsByLevel(NODE tree, BiFunction<List<NODE>, Integer, Boolean> fn) {
        Set<CODE> visitedCodes = new HashSet<>();
        List<NODE> queue = new LinkedList<>();
        int level = 0;
        queue.add(tree);
        while (!queue.isEmpty()) {
            for (NODE n : queue) {
                if (!visitedCodes.add(codeGetter.apply(n))) {
                    throw new RuntimeException("multiple code: " + codeGetter.apply(tree) + ", maybe there is circular reference!");
                }
            }
            if (!fn.apply(queue, level++)) {
                return;
            }
            queue = queue.stream().filter(it -> childrenGetter.apply(it) != null)
                    .flatMap(it -> childrenGetter.apply(it).stream())
                    .collect(Collectors.toList());
        }
    }

    /**
     * 快速构建一颗树。
     * 仅需两次遍历，时间复杂度为o(n)
     * !会修改children属性
     * @return
     */
    public NODE treeify(Map<CODE, NODE> nodes) {
        nodes.values().forEach(it->{
            Collection<NODE> children = childrenGetter.apply(it);
            if(children!=null && !children.isEmpty()){
                childrenSetter.accept(it,childrenCollectionFactory.get());
            }
        });
        AtomicReference<NODE> tree = new AtomicReference<>();
        nodes.forEach((code, node) -> {
            CODE parentCode = parentCodeGetter.apply(node);
            NODE parentNode = nodes.get(parentCode);
            if (parentNode == null) {
                if (!tree.compareAndSet(null, node)) {
                    throw new RuntimeException("too many tree nodes!");
                }
                return;
            }
            Collection<NODE> children = childrenGetter.apply(parentNode);
            if (children == null) {
                children = childrenCollectionFactory.get();
                childrenSetter.accept(parentNode, children);
            }
            children.add(node);
        });
        return tree.get();
    }

    /**
     * !会修改children属性
     * @param nodes
     * @return
     */
    public NODE treeify(Collection<NODE> nodes) {
        return treeify(nodes.stream().map(it -> {
            Collection<NODE> children = childrenGetter.apply(it);
            if (children != null && !children.isEmpty()) {
                childrenSetter.accept(it,childrenCollectionFactory.get());
            }
            return it;
        }).collect(Collectors.toMap(codeGetter, Function.identity(), (oldOne, newOne) -> {
            throw new RuntimeException("duplicate node(code=" + codeGetter.apply(newOne) + ")");
        }, () -> new LinkedHashMap<>())));
    }
    /**
     * !会修改children属性
     * @param nodes
     * @return
     */
    public NODE treeify(Collection<NODE> nodes, BinaryOperator<NODE> mergeFunction) {
        return treeify(nodes.stream().map(it -> {
            Collection<NODE> children = childrenGetter.apply(it);
            if (children != null && !children.isEmpty()) {
                childrenSetter.accept(it,childrenCollectionFactory.get());
            }
            return it;
        }).collect(Collectors.toMap(codeGetter, Function.identity(), mergeFunction, () -> new LinkedHashMap<>())));
    }

    /**
     * 合并两个树。如果两颗树有相同code的节点，前者的节点会被丢弃。
     * 也就是说，该方法认为该节点发生了迁移，迁移的位置以后者为准。
     * !会修改children属性
     * @param oldTree
     * @param newTree
     * @return
     */
    public NODE mergeTrees(NODE oldTree, NODE newTree) {
        Map<CODE, NODE> nodes = nodeMap(oldTree);
        nodes.putAll(nodeMap(newTree));
        return treeify(nodes.values());
    }

    public Map<CODE, NODE> nodeMap(NODE tree) {
        Map<CODE, NODE> map = new LinkedHashMap<>();
        bfsAll(tree, (node, level) -> {
            map.put(codeGetter.apply(node), node);
        });
        return map;
    }

    /**
     * 输出漂亮的格式
     *
     * @param tree   根节点
     * @param indent 缩进字符串
     * @return
     */
    public String toPrettyString(NODE tree, String indent) {
        return toPrettyString(tree, indent, Object::toString);
    }

    public String toPrettyString(NODE tree, String indent, Function<NODE, String> toStr) {
        StringBuilder sb = new StringBuilder();
        List<String> indents = new ArrayList<>();
        indents.add("");
        dfsAllWithPreOrder(tree, (node, level) -> {
            if (indents.size() <= level) {
                indents.add(indents.get(level - 1) + indent);
            }
            sb.append(indents.get(level)).append(toStr.apply(node)).append("\n");
        });
        return sb.toString();
    }

    public NODE find(NODE tree, BiPredicate<NODE, Integer> predicate) {
        AtomicReference<NODE> ref = new AtomicReference<>();
        bfs(tree, (node, level) -> {
            if (predicate.test(node, level)) {
                ref.set(node);
                return false;
            } else {
                return true;
            }
        });
        return ref.get();
    }

    public List<NODE> findAll(NODE tree, BiPredicate<NODE, Integer> predicate) {
        List<NODE> list = new ArrayList<>();
        bfsAll(tree, (node, level) -> {
            if (predicate.test(node, level)) {
                list.add(node);
            }
        });
        return list;
    }


}
